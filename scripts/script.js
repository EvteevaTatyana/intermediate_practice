document.querySelector('#btnSubmitReg').addEventListener('click', (event) => {
    event.preventDefault();

    const email = document.querySelector('#emailReg');
    const labelEmail = document.querySelector('#emailRegLabel');
    const starEmail = document.querySelector('#emailRegStar');
    const textEmail = document.querySelector('#regEmailTextReg');

    const password = document.querySelector('#passwordReg');
    const labelPassword = document.querySelector('#passwordRegLabel');
    const starPassword = document.querySelector('#passwordRegStar');
    const textPassword = document.querySelector('#regPasswordTextReg');

    const checkbox = document.querySelector('#checkboxReg');
    const borderCheckbox = document.querySelector('.checkbox-reg');
    const starCheckbox = document.querySelector('#checkboxRegStar');
    const textCheckbox = document.querySelector('#regCheckboxTextReg');

    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let emailValid = re.test(String(email.value).toLowerCase());

    let validate = true;

    if (emailValid) {
        email.style.borderColor = '';
        labelEmail.style.color = '';
        starEmail.style.color = '';
        textEmail.style.opacity = 0;
    } else {
        email.style.borderColor = 'red';
        labelEmail.style.color = 'red';
        starEmail.style.color = 'red';
        textEmail.style.opacity = 1;
        validate = false;
    }

    if (password.value.lenght >= 8) {
        password.style.borderColor = '';
        labelPassword.style.color = '';
        starPassword.style.color = '';
        textPassword.style.opacity = 0;
    } else {
        password.style.borderColor = 'red';
        labelPassword.style.color = 'red';
        starPassword.style.color = 'red';
        textPassword.style.opacity = 1;
        validate = false;
    }

    if (checkbox.checked) {
        borderCheckbox.style.borderColor = '';
        starCheckbox.style.color = '';
        textCheckbox.style.opacity = 0;
    } else {
        borderCheckbox.style.borderColor = 'red';
        starCheckbox.style.color = 'red';
        textCheckbox.style.opacity = 1;
        validate = false;
    }

    if (validate) {
        const data = {
            email: email.value.toLowerCase(),
            password: password.value
        };

        if (localStorage.getItem('data')) {
            let localData = localStorage.getItem('data');
            localData = JSON.parse(localData);

            if (localData.filter((user) => user.email === email.value).lenght) {
                console.log('Пользователь с таким адресом уже существуют');
            } else {
                localData.push(data);
                localStorage.setItem('data', JSON.stringify(localData));
                console.log('Пользователь сохранен!');
            }

        } else {
            localStorage.setItem('data', JSON.stringify([data]));
            console.log('Пользователь сохранен!');
        }
    }
})

document.querySelector('#btnSubmitReg').addEventListener('click', (event) => {
    event.preventDefault();

    const email = document.querySelector('#emailAut');
    const labelEmail = document.querySelector('#emailAutLabel');
    const starEmail = document.querySelector('#emailAutStar');
    const textEmail = document.querySelector('#regEmailTextAut');

    const password = document.querySelector('#passwordAut');
    const labelPassword = document.querySelector('#passwordAutLabel');
    const starPassword = document.querySelector('#passwordAutStar');
    const textPassword = document.querySelector('#regPasswordTextAut');

    const checkbox = document.querySelector('#checkboxAut');
    const borderCheckbox = document.querySelector('.checkmark-aut');
    const starCheckbox = document.querySelector('#checkboxAutStar');
    const textCheckbox = document.querySelector('#regCheckboxTextAut');

    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let emailValid = re.test(String(email.value).toLowerCase());

    let validate = true;

    if (emailValid) {
        email.style.borderColor = '';
        labelEmail.style.color = '';
        starEmail.style.color = '';
        textEmail.style.opacity = 0;
    } else {
        email.style.borderColor = 'red';
        labelEmail.style.color = 'red';
        starEmail.style.color = 'red';
        textEmail.style.opacity = 1;
        validate = false;
    }

    if (password.value.lenght >= 8) {
        password.style.borderColor = '';
        labelPassword.style.color = '';
        starPassword.style.color = '';
        textPassword.style.opacity = 0;
    } else {
        password.style.borderColor = 'red';
        labelPassword.style.color = 'red';
        starPassword.style.color = 'red';
        textPassword.style.opacity = 1;
        validate = false;
    }

    if (checkbox.checked) {
        borderCheckbox.style.borderColor = '';
        starCheckbox.style.color = '';
        textCheckbox.style.opacity = 0;
    } else {
        borderCheckbox.style.borderColor = 'red';
        starCheckbox.style.color = 'red';
        textCheckbox.style.opacity = 1;
        validate = false;
    }

    if (validate) {

        if (localStorage.getItem('data')) {
            let localData = localStorage.getItem('data');
            localData = JSON.parse(localData);
            
            if (localData.filter((user) => user.email === email.value).lenght) {
                if (localData.filter((user) => user.email === email.value)[0].password === password.value) {
                    alert('Вы успешно авторизованы!')
                } else {
                    console.log('Введены не верные данные');
                }
            } else {
                console.log('Введены не верные данные');
            }
    
        } else {
            console.log('Введены не верные данные');
        }
    }

})

document.querySelector('#toggle-btn-aut').addEventListener('click', (event) => {
    event.preventDefault();
    document.querySelector('.form-aut').style.display = 'none';
    document.querySelector('.form-reg').style.display = 'block';
})

document.querySelector('#toggle-btn-red').addEventListener('click', (event) => {
    event.preventDefault();
    document.querySelector('.form-aut').style.display = 'block';
    document.querySelector('.form-reg').style.display = 'none';
})
